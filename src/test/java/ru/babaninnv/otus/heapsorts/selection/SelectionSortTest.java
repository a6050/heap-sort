package ru.babaninnv.otus.heapsorts.selection;

import org.junit.jupiter.api.DisplayName;
import ru.babaninnv.otus.heapsorts.test.SortingTestFactory;

@DisplayName("Selection Sort")
class SelectionSortTest extends SortingTestFactory {
    @Override
    public void runSort(int[] array) {
        new SelectionSort().sort(array);
    }
}