package ru.babaninnv.otus.heapsorts.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.Timeout;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public abstract class SortingTestFactory {

	@TestFactory
	@DisplayName("Random array")
	@Timeout(value = 2, unit = TimeUnit.MINUTES)
	public Stream<DynamicTest> testWithRandomArray() {
		return DynamicTest.stream(
				new TestDataSource("data/0.random"),
				SortingTestFactory::generateDisplayName,
				this::executeTest
		);
	}

	@TestFactory
	@DisplayName("Digits array")
	@Timeout(value = 2, unit = TimeUnit.MINUTES)
	public Stream<DynamicTest> testWithDigitsArray() {
		return DynamicTest.stream(
				new TestDataSource("data/1.digits"),
				SortingTestFactory::generateDisplayName,
				this::executeTest
		);
	}

	@TestFactory
	@DisplayName("Sorted array")
	@Timeout(value = 2, unit = TimeUnit.MINUTES)
	public Stream<DynamicTest> testWithSortedArray() {
		return DynamicTest.stream(
				new TestDataSource("data/2.sorted"),
				SortingTestFactory::generateDisplayName,
				this::executeTest
		);
	}

	@TestFactory
	@DisplayName("Reversed array")
	@Timeout(value = 2, unit = TimeUnit.MINUTES)
	public Stream<DynamicTest> testWithReversedArray() {
		return DynamicTest.stream(
				new TestDataSource("data/3.revers"),
				SortingTestFactory::generateDisplayName,
				this::executeTest
		);
	}

	private void executeTest(TestData testData) {
		final int[] expectedResult = toArray(testData.getOutput().get(0));
		final int[] array = toArray(testData.getInput().get(1));

		runSort(array);

		assertArrayEquals(expectedResult, array);
	}

	private static String generateDisplayName(TestData testData) {
		return "with length " + testData.getInput().get(0);
	}

	private static int[] toArray(String string) {
		return Arrays.stream(string.split(" "))
				.mapToInt(Integer::parseInt)
				.toArray();
	}

	public abstract void runSort(int[]  array);

}
