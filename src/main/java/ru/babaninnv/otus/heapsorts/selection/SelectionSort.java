package ru.babaninnv.otus.heapsorts.selection;

public class SelectionSort {

    public void sort(int[] array) {
        final int size = array.length;

        for (int i = 0; i < size - 1; i++) {
            int minItemIndex = i;

            for (int j = i + 1; j < size; j++) {
                if (array[minItemIndex] > array[j]) {
                    minItemIndex = j;
                }
            }

            if (minItemIndex != i) {
                swap(array, i, minItemIndex);
            }
        }
    }

    private void swap(int[] array, int a, int b) {
        int swap = array[a];
        array[a] = array[b];
        array[b] = swap;
    }
}
