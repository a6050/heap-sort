package ru.babaninnv.otus.heapsorts.heap;

import org.junit.jupiter.api.DisplayName;
import ru.babaninnv.otus.heapsorts.test.SortingTestFactory;

@DisplayName("Heap Sort")
class HeapSortTest extends SortingTestFactory {

    @Override
    public void runSort(int[] array) {
        new HeapSort().sort(array);
    }
}